class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products do |t|
      t.string :title
      t.text :body
      t.boolean :published
      t.string :image_location

      t.timestamps
    end
  end
end
