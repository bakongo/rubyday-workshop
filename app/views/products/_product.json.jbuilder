json.extract! product, :id, :title, :body, :published, :image_location, :created_at, :updated_at
json.url product_url(product, format: :json)